const fs = require('fs');
const util = require('util')

const hierarchicalSort = (data, getSortValueFn) => {
    sortMetric = getSortValueFn();
    rows = data.split('\n')
    let headers = rows[0]
    props_and_metrics = rows[0].split('|')

    const sortArrOfObjectsUsingValues = (arr) => {
        arr.sort(function (a, b) {
            let aValue = a[Object.keys(a)[0]]
            let bValue = b[Object.keys(b)[0]]
            return bValue - aValue
        })
        return arr
    }

    const sortArrOfObjectsUsingKeys = (arr) => {
        arr.sort(function (a, b) {
            let aValue = parseFloat(Object.keys(a)[0])
            let bValue = parseFloat(Object.keys(b)[0])
            return bValue - aValue
        })
        return arr
    }

    //for $total$total... or $totalSmth
    const isSameWordRepeated = (str, word) => {
        str = str.replace(/\|/g, '')
        let division = str.length / word.length
        if (str.length % word.length === 0) {
            let candidate = ''
            for (let idx = 0; idx < division; idx++) {
                candidate += word
            }
            if (candidate === str) {
                return true
            }
            return false
        }
        return false
    }

    const reducer = (acc, current_value, idx) => {
        if (current_value.indexOf('property') >= 0) {
            acc['props'].push(current_value)
            acc['propsIndexes'][current_value] = idx
        } else {
            acc['metrics'].push(current_value)
            acc['metricsIndexes'][current_value] = idx
        }
        return acc
    }
    const {props, metrics, propsIndexes, metricsIndexes} = props_and_metrics.reduce(reducer,
        {
            'props': [],
            'metrics': [],
            'propsIndexes': {},
            'metricsIndexes': {}
        }
    )

    let sub_out = {};
    let subtotals = {};
    let subtotals_metric = []
    let out = []

    const assign_new_positions_to_obj = (dstObj, key, subarray) => {
        if (!(subarray in subtotals[key])) {
            return
        }
        for (let si = 0; si < subtotals[key][subarray].length; si++) {
            let subKey = rows[Object.keys(subtotals[key][subarray][si])]
            dstObj.push(subKey)
        }
    }

    rows[0] = rows[0].split('|')

    for (let idx = 1; idx < rows.length; idx++) {
        let row = rows[idx]
        let rowValues = row.split('|')
        if (rowValues.length <= 1) {
            continue
        }
        let keyString = ''
        for (let j = 0; j < rowValues.length; j++) {
            if (rows[0][[j]] in propsIndexes) {
                keyString += rowValues[j] + "|"
            }
        }
        keyString = keyString.substring(0, keyString.length - 1)
        sub_out[keyString] = rowValues[metricsIndexes[sortMetric]];
        let subKey = keyString.substring(0, keyString.lastIndexOf('|'))
        let numericalValue = parseFloat(sub_out[keyString])
        if (!(subKey in subtotals)) {
            subtotals[subKey] = {}
            subtotals[subKey]['positions'] = []
            //for $total occurrence
            subtotals[subKey]['special_positions'] = []
            subtotals[subKey]['subtotal'] = numericalValue
        } else {
            subtotals[subKey]['subtotal'] += numericalValue
        }

        let obj = {}
        obj[parseInt(idx)] = numericalValue

        let indexOfTotal = keyString.indexOf('$total')

        if (indexOfTotal === 0) {
            if (isSameWordRepeated(keyString, '$total')) {
                subtotals[subKey]['special_positions'].push(obj)
            } else {
                subtotals[subKey]['positions'].push(obj)
            }
        } else if (indexOfTotal !== -1) {
            subtotals[subKey]['special_positions'].push(obj)
        } else {
            subtotals[subKey]['positions'].push(obj)
        }
        subtotals[subKey]['positions'] = sortArrOfObjectsUsingValues(subtotals[subKey]['positions'])
        subtotals[subKey]['special_positions'] = sortArrOfObjectsUsingValues(subtotals[subKey]['special_positions'])
    }

    for (let key in subtotals) {
        if (!subtotals.hasOwnProperty(key)) {
            continue
        }
        //prefix of $total, go through $total keys first
        if (key.indexOf('$total') === 0) {
            assign_new_positions_to_obj(out, key, 'special_positions')
            assign_new_positions_to_obj(out, key, 'positions')
            delete subtotals[key]
            continue
        }
        let obj = {}
        let subtotalNum = subtotals[key].subtotal
        obj[subtotalNum] = key
        subtotals_metric.push(obj)
    }
    subtotals_metric = sortArrOfObjectsUsingKeys(subtotals_metric)


    for (let idx = 0; idx < subtotals_metric.length; idx++) {
        let obj = subtotals_metric[idx]
        let key = obj[Object.keys(obj)[0]]
        if (key.indexOf('$total') !== -1 || subtotals[key].special_positions.length) {
            assign_new_positions_to_obj(out, key, 'special_positions')
        }
        if (!subtotals[key].positions.length) {
            continue
        }
        assign_new_positions_to_obj(out, key, 'positions')
    }


    // console.log(util.inspect(sub_out, {showHidden: false, depth: null}))
    // console.log(util.inspect(subtotals, {showHidden: false, depth: null}))
    //
    // console.log('**********')
    // console.log(util.inspect(out, {showHidden: false, depth: null}))


    return headers + '\n' + out.join('\n')
}


data = fs.readFileSync(
    process.argv[2] ? process.argv[2] : './data.txt',
    {
        encoding: 'utf8'
    }
);

dt = hierarchicalSort(data, (rows) => 'net_sales');
console.log(dt)